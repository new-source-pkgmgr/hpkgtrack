# hpkgtrack

## About

hpkgtrack is a package "manager" that tracks files you install and groups them into
packages which can be inspected or removed with ease. This way you can install
applications from source with "make install" and remove those files without issue.

The idea for the program came from the "deck" tool which similarly tracks installed
files and allows you to remove packages you've installed. However, hpkgtrack uses
the INotify system for detecting file creation, rather than hashing all of tracked
directories and their contents.

## Configuration

To use hpkgtrack you must create a configuration file at /etc/pkgtrack.conf where
you specify where the package database will be located and which directories will be
tracked. Alternatively you can pass the -c <path> flag to load a custom config file.

The configuration syntax is simple, you define the database location with 
"-db <path-to-file>" and then you can define as many binary and configuration
file paths using "-bin <path-to-dir>" and "-config <path-to-dir>" as you want.
Each binary path and config path must be on a separate line and must end with a "/".
You cannot define multiple database locations.

Binary directories would be directories that contain immutable application
executables or data. Configuration directories would be directories that
contain user-changeable information that the user might wish to keep
even if they want to remove the package.

### Example configuration file (example.conf)
```
-db /var/lib/pkgtrack/pkgtrack.db
-bin /usr/local/bin/
-bin /home/user/.bin/
-config /usr/local/share/
-config /home/user/.config/
```

## Usage

### Installing a package

To install a package you run the command "hpkgtrack install package-name command"
where "package-name" is the name of your package and "command" is a shell command
that installs the program. For example, to install a program that uses "make", you
might do the following: "hpkgtrack install castawesome 'make install'".

You can also define an optional version string for a given package with the -v flag.
Optional flags are given as the last arguments to hpkgtrack. The version flag can
also be omitted and "0.1" is used as the default.

### Listing installed packages

To view what packages have been installed you can run "hpkgtrack list". This will
give you a list of packages in the current package database, along with their
given version strings.


### Inspecting a package

To view the details of an installed package you can run "hpkgtrack inspect package".
This allows you to see what files are attached to the package and which ones are
binary files and which ones are considered configuration files.

### Removing a package

To remove a package you can run "hpkgtrack remove package". This will remove the
package from the database and also remove all of the binaries associated with the
package. All the files that are considered configuration files are still kept
untouched.

### Purging a package

To completely remove a package and all of the files associated with it, you can
run "hpkgtrack purge package". This removes the package from the database along
with all the configuration and binary files that were tied to the package.


