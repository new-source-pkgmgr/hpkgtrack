module DataTypes where

-- Package name, version, binaries, configs
data Package = Package 
    {
      name :: String 
    , pkgVersion :: String 
    , binaries :: [String] 
    , configs :: [String]
    } deriving (Show, Read)

-- Command, Package name, Build process, Version, Config
data MyArgs = MyArgs 
    {
      command :: String
    , package :: String
    , build :: String 
    , argVersion :: String 
    , config :: String 
    } deriving Show

-- DB path, Binary paths, Config paths
data MyConfigs = MyConfigs 
    {
        dbPath :: String 
      , binPaths :: [String] 
      , confPaths :: [String]
    } deriving Show


