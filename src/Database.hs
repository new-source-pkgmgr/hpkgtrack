module Database where

import System.INotify
import System.Console.ArgParser
import System.Process
import System.IO
import System.Directory  

import Data.IORef
import Data.List
-- import Data.List.Split

import Control.Monad
-- import Control.Applicative
import Control.Concurrent

import DataTypes

readPackages :: MyConfigs -> IO ([Package])
readPackages conf = do
    -- putStrLn "Reading package database..."
    fileExists <- doesFileExist (dbPath conf)
    if fileExists
    then do
        text <- readFile (dbPath conf)

        let pkgs = map (\x -> read x :: Package) (lines text)

        return pkgs
    else return []


writePackages :: MyConfigs -> [Package] -> IO ()
writePackages conf pkgs = do
    -- putStrLn "Writing package database..."
    h <- openFile ((dbPath conf) ++ ".new") WriteMode
    let texts = map (show) pkgs
    
    forM_ texts $ \pkg -> do
        hPutStrLn h pkg
    
    hClose h
    
    renameFile ((dbPath conf) ++ ".new") (dbPath conf)

    return ()

appendPackage :: MyConfigs -> Package -> IO ()
appendPackage conf pkg = do
    oldPkgs <- readPackages conf
    writePackages conf (pkg : oldPkgs)

purgePackage :: MyConfigs -> Package -> IO ()
purgePackage conf pkg = do
    -- putStrLn "Removing package..."
    pkgs <- readPackages conf
    
    let pkgToRemove = find (\x -> (name x) == (name pkg)) pkgs

    case pkgToRemove of
        Just package -> do
            forM_ (binaries package) $ \path -> do
                fileExists <- doesFileExist path
                dirExists <- doesDirectoryExist path
                if fileExists
                    then removeFile path
                else if dirExists
                    then removeDirectoryRecursive path
                else return ()
            forM_ (configs package) $ \path -> do
                fileExists <- doesFileExist path
                dirExists <- doesDirectoryExist path
                if fileExists
                    then removeFile path
                else if dirExists
                    then removeDirectoryRecursive path
                else return ()
        _ -> return ()

    let newPkgs = filter (\x -> (name x) /= (name pkg)) pkgs
    writePackages conf newPkgs

removePackage :: MyConfigs -> Package -> IO ()
removePackage conf pkg = do
    -- putStrLn "Removing package..."
    pkgs <- readPackages conf
    
    let pkgToRemove = find (\x -> (name x) == (name pkg)) pkgs

    case pkgToRemove of
        Just package -> do
            forM_ (binaries package) $ \path -> do
                fileExists <- doesFileExist path
                dirExists <- doesDirectoryExist path
                if fileExists
                    then removeFile path
                else if dirExists
                    then removeDirectoryRecursive path
                else return ()
        _ -> return ()

    let newPkgs = filter (\x -> (name x) /= (name pkg)) pkgs
    writePackages conf newPkgs


