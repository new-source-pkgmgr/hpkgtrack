module Main where

import GHC.IO.Encoding
import System.INotify
import System.Console.ArgParser
import System.Process
import System.IO
import System.Directory  

import Data.IORef
import Data.List
-- import Data.List.Split

import Control.Monad
-- import Control.Applicative
import Control.Concurrent

import Database
import DataTypes

-- Add an "/" to the end of a path, if necessary 
directorize :: String -> String
directorize path = if "/" `isSuffixOf` path
                   then path
                   else path ++ "/"

getConfigs :: MyArgs -> IO (MyConfigs)
getConfigs args = do
    if (config args) == "/etc/pkgtrack.conf"
        then do
            userXDGConfig <- getXdgDirectory XdgConfig "pkgtrack"
            homeConfigExists <- doesFileExist (userXDGConfig ++ "/pkgtrack.conf")
            if homeConfigExists
                then readConfigs (userXDGConfig ++ "/pkgtrack.conf")
            else readConfigs (config args)
    else readConfigs (config args)

readConfigs :: FilePath -> IO (MyConfigs)
readConfigs path = do
    text <- readFile path
    let confPaths = map directorize $ 
            map (\x -> drop (length "-config ") x) $ 
            filter (\x -> "-config" `isPrefixOf` x) (lines text)
    let binPaths = map directorize $ 
            map (\x -> drop (length "-bin ") x) $ 
            filter (\x -> "-bin" `isPrefixOf` x) (lines text)
    let dbPath = drop (length "-db ") $ 
            head $ filter (\x -> "-db" `isPrefixOf` x) (lines text)
    return (MyConfigs dbPath binPaths confPaths)

addFileToIORef :: IORef ([FilePath]) -> FilePath -> IO ()
addFileToIORef ref file = do
    atomicModifyIORef ref $ \paths -> ((file : paths), ())

myAddINotifyWatch :: INotify -> IORef ([FilePath]) -> FilePath -> IO ()
myAddINotifyWatch notify ref path = do
    listDir <- listDirectory path

    forM_ listDir $ \file -> do
        isDirectory <- doesDirectoryExist (path ++ file)
        if isDirectory
            then myAddINotifyWatch notify ref (path ++ file ++ "/")
        else return ()

    watch <- addWatch notify [Create, Move] path $ \event ->
        case event of
            Created _ file -> do
                isDirectory <- doesDirectoryExist (path ++ file)
                if isDirectory
                    then myAddINotifyWatch notify ref (path ++ file ++ "/")
                else return ()    
                addFileToIORef ref (path ++ file)
            MovedIn _ file _ -> addFileToIORef ref (path ++ file)
            _ -> return ()
    return ()

installPackage :: MyArgs -> MyConfigs -> IO ()
installPackage args conf = do
    putStrLn $ "Installing " ++ (package args) ++ " using " ++ (build args) 
    
    purgePackage conf (Package (package args) "" [] [])
  
    confFiles <- newIORef ([] :: [FilePath])
    binFiles <- newIORef ([] :: [FilePath])
    
    notify <- initINotify
    
    -- Create an inotify watch for all binary paths
    forM_ (binPaths conf) (\path -> myAddINotifyWatch notify binFiles path)
    
    -- Create an inotify watch for all config paths
    forM_ (confPaths conf) (\path -> myAddINotifyWatch notify confFiles path)
   
    -- Give the system some time to start catching files and run command 
    threadDelay (5 * 1000 * 1000)

    handle <- runCommand (build args)
    
    waitForProcess handle

    threadDelay (5 * 1000 * 1000)

    doneConfs <- readIORef confFiles
    doneBins <- readIORef binFiles

    print doneConfs
    print doneBins
    let newPackage = Package (package args) (argVersion args) doneBins doneConfs
    
    appendPackage conf newPackage

    return ()

listPackages :: MyArgs -> MyConfigs -> IO ()
listPackages args conf = do
    putStrLn "Following packages have been installed:"
    pkgs <- readPackages conf
    forM_ pkgs $ \pkg -> do
        putStrLn $ "  " ++ (name pkg) ++ " - " ++ (pkgVersion pkg)
       
    return ()

inspectPackage :: MyArgs -> MyConfigs -> IO ()
inspectPackage args conf = do
    putStrLn $ "Package details for " ++ (package args)
    pkgs <- readPackages conf
    let pkg = head $ filter (\x -> (name x) == (package args)) pkgs
    
    putStrLn $ "  " ++ (name pkg) ++ " - " ++ (pkgVersion pkg)
    
    forM_ (binaries pkg) $ \bin -> do
        putStrLn $ "\tBinary: " ++ bin
    forM_ (configs pkg) $ \conf -> do
        putStrLn $ "\tConfig: " ++ conf
    
    return ()

uninstallPackage :: MyArgs -> MyConfigs -> IO ()
uninstallPackage args conf = do
    putStrLn $ "Removing package: " ++ (package args)
    removePackage conf (Package (package args) "" [] [])

updatePackage :: MyArgs -> MyConfigs -> IO ()
updatePackage args conf = do
    uninstallPackage args conf
    installPackage args conf
    putStrLn $ "Package updated."

clearPackage :: MyArgs -> MyConfigs -> IO ()
clearPackage args conf = do
    putStrLn $ "Purging package: " ++ (package args)
    purgePackage conf (Package (package args) "" [] [])

app :: MyArgs -> IO ()
app args = do
    conf <- getConfigs args
    case command args of
        "install" -> installPackage args conf
        "list" -> listPackages args conf
        "inspect" -> inspectPackage args conf
        "remove" ->  uninstallPackage args conf
        "update" -> updatePackage args conf
        "purge" -> clearPackage args conf
        _ -> putStrLn $ "Unrecognized command '" ++ (command args) ++ "'"

myArgParser :: ParserSpec MyArgs
myArgParser = MyArgs
  `parsedBy` reqPos "command"                    `Descr` "pkgtrack command to issue"
  `andBy` optPos "none" "pkg-name"               `Descr` "package name" 
  `andBy` optPos "make installl" "cmd"           `Descr` "command to run to install"
  `andBy` optFlag "0.1" "version"                `Descr` "version string"
  `andBy` optFlag "/etc/pkgtrack.conf"  "config" `Descr` "configuration file to load"

main :: IO ()
main = do
    setLocaleEncoding utf8
    withParseResult myArgParser app
