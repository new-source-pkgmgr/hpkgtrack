FROM debian:latest

RUN apt-get update && apt-get install -y git haskell-stack
RUN git clone https://gitlab.com/new-source-pkgmgr/hpkgtrack.git
WORKDIR ./hpkgtrack
RUN stack build --ghc-options='-optl-static -optl-pthread' --force-dirty

CMD git pull origin master && stack build --ghc-options='-optl-static -optl-pthread' --force-dirty && stack install
